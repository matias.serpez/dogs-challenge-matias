/**
 * COMMON PROVIDER TYPES
 */

declare type ActionMap<M extends { [index: string]: any }> = {
    [Key in keyof M]: M[Key] extends undefined
    ? {
        type: Key;
    }
    : {
        type: Key;
        payload: M[Key];
    }
};

/**
 *  APPLICATION PROVIDER TYPES
 */

declare type AppProviderInitialState = {
    dogsData: DogsDataProviderType
};

declare type DogsDataProviderType = {
    team: {
        breeds: {} | Record<string, Array<string>>
    },
    dogs: {} | Record<string, Record<number, Array<string>>>
};
