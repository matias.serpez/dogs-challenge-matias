import MyTeam from './my-team';
import Dogs from './dogs';
import Breeds from './breeds';

const Containers = {
    MyTeam,
    Dogs,
    Breeds
}

export default Containers;