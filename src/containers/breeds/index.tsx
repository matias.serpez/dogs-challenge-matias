import _map from 'lodash/map'
import styled from '@emotion/styled'

import { useFetchBreeds } from "../../dal";
import { GridItem } from "../../components/GridItem";
import { LoadingIndicator } from '../../components/LoadingIndicator';


export default function Breeds() {
    const { data, status, isLoading } = useFetchBreeds();

    if (status === 'error') return <h1>Error</h1>
    if (isLoading) return <LoadingIndicator />

    const formatedData = _map(data, (item, key) => key)

    // TODO: Change inline styles
    return (
        <StyledContainer>
            {formatedData.map(breedName => (
                <GridItem
                    title={breedName}
                    link={`/dogs/${breedName}`}
                    linkLabel="Vew all"
                />
            ))}
        </StyledContainer>
    )
}

const StyledContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
`