import { useContext } from "react";
import { AppContext } from "../../providers/app-provider/context";
import { DogDataTypes } from "../../providers/app-provider/types";
import { useFormatedTeam } from "../../utils/hooks";


export function useMyTeam() {
    const { state: { dogsData: { team } }, dispatch } = useContext(AppContext);
    const { formatedTeam, teamSize } = useFormatedTeam(team);

    const onRemoveDogClick = (dogImg: string, breedName: string) => {
        dispatch({ type: DogDataTypes.RemoveDogFromTeam, payload: { dogImg, breedName, } })
    }

    return {
        team: formatedTeam,
        teamSize,
        onRemoveDogClick,
    }
}