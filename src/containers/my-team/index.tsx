import _map from 'lodash/map';
import styled from '@emotion/styled'

import { useMyTeam } from "./useMyTeam";
import { GridItem } from '../../components/GridItem';
import { FlexContainer } from '../../components/FlexContainer';
import { StaticInfo } from '../../components/StaticInfo';

export default function MyTeam() {
    const { team, teamSize, onRemoveDogClick } = useMyTeam();

    if (teamSize === 0) return <StaticInfo text="No team yet!" />

    return (
        <FlexContainer>
            {_map(team, (breed) => {
                if (!breed.images.length) return null;
                return (
                    <StyledItemContainer>
                        <StyledItemTitle>{breed.name}</StyledItemTitle>
                        <FlexContainer>
                            {breed.images.map((image, idx) => {
                                const onRemoveDogClickHandler = () => onRemoveDogClick(image, breed.name);
                                return (
                                    <GridItem
                                        key={`dog_team_${breed.name}_${idx}`}
                                        imageUrl={image}
                                        btnLabel="Remove from Team"
                                        btnClick={onRemoveDogClickHandler}
                                    />
                                )
                            })}
                        </FlexContainer>
                    </StyledItemContainer>
                );
            })}
        </FlexContainer>
    )
}

const StyledItemContainer = styled.div`
    width: 100%;
`;

const StyledItemTitle = styled.h2`
    width: 100%;
    text-transform: capitalize;
    border-bottom: 1px solid #dadada;
    padding-bottom: 8px;
`;