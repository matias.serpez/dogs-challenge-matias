import { useContext, useEffect } from "react";
import { get as _get, flatten as _flatten } from "lodash";
import { useSnackbar } from 'react-simple-snackbar'

import { useFetchDogs } from "../../dal";
import { AppContext } from "../../providers/app-provider/context";
import { DogDataTypes } from "../../providers/app-provider/types";
import { useFormatedTeam } from "../../utils/hooks";

import { MAX_DOGS_PER_BREED_PER_TEAM, MAX_TOTAL_DOGS_PER_TEAM } from "../../constants";

export function useDogs(breedName: string, page: number) {
    const { status, isLoading, data } = useFetchDogs(breedName, page);
    const { dispatch, state: { dogsData: { dogs, team } } } = useContext(AppContext);
    const { teamSize, formatedTeam } = useFormatedTeam(team);
    const [openErrorSnack] = useSnackbar({ position: 'top-right', style: { background: '#b2102f', color: 'white' } })
    const [openSuccessSnack] = useSnackbar({ position: 'top-right', style: { background: '#357a38', color: 'white' } })


    // Once api is fetched, set fetched page to reducer.
    useEffect(() => {
        const isPageOnState = _get(dogs, `${breedName}.${page}`, [])?.length > 0;

        if (data !== undefined && !isPageOnState) {
            dispatch({
                type: DogDataTypes.AddDogs,
                payload: {
                    breedName,
                    page,
                    dogImgs: data
                }
            })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [data, page, breedName])

    // Click handler to add to team click.
    const onDogClick = (dogImg: string) => {
        let validLengthBreed = true;
        let alreadyInTeam = false;

        formatedTeam.forEach((item) => {
            if (item.name === breedName) {
                // If it's valid, still checking for the max length per breed
                if (validLengthBreed) {
                    validLengthBreed = item.images.length < MAX_DOGS_PER_BREED_PER_TEAM
                }
                // Check if it's already added to team.
                if (!alreadyInTeam) {
                    alreadyInTeam = item.images.includes(dogImg)
                }
            }
        })

        if (alreadyInTeam) {
            openErrorSnack("Error: Already in team.");
        } else if (!validLengthBreed) {
            // Check if breed has availability to add one more dog.
            openErrorSnack(`Error: Max ${MAX_DOGS_PER_BREED_PER_TEAM} per breed.`);
        } else if (teamSize >= MAX_TOTAL_DOGS_PER_TEAM) {
            openErrorSnack(`Error: Max dogs in total reached (${MAX_TOTAL_DOGS_PER_TEAM}).`);
        } else {
            openSuccessSnack("Added to team!");
            dispatch({ type: DogDataTypes.AddDogToTeam, payload: { breedName, dogImg } })
        }

    }

    // Take from reducer the right data.
    const finalData: Array<string> = _flatten(_get(dogs, `${breedName}`, []));

    return {
        data: finalData,
        status,
        isLoading,
        onDogClick,
    }
}