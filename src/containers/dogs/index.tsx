import { memo } from 'react';
import { useParams } from 'react-router-dom';

import { useDogs } from './useDogs';

import { GridItem } from '../../components/GridItem';
import { FlexContainer } from '../../components/FlexContainer';
import { useScrollPagination } from '../../utils/hooks';
import { LoadingIndicator } from '../../components/LoadingIndicator';

export default memo(function Dogs() {
    const { id: breedName } = useParams<{ id: string }>()
    const { page } = useScrollPagination();
    const { status, isLoading, data, onDogClick } = useDogs(breedName, page);

    if (status === 'error') return <h1>Error</h1>


    return (
        <FlexContainer>
            {data?.map((dogImg, idx) => {

                const onClickHandler = () => onDogClick(dogImg);

                return <GridItem key={`dog_${idx}`} btnClick={onClickHandler} btnLabel="Add to Team" imageUrl={dogImg} />
            })}
            {isLoading && <LoadingIndicator />}
        </FlexContainer>
    )
})
