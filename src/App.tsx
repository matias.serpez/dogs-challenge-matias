import { Route, Switch, BrowserRouter } from "react-router-dom";
import { QueryClient, QueryClientProvider } from 'react-query'
import SnackbarProvider from 'react-simple-snackbar'

import { NotFound } from './components/NotFound';
import { Layout } from './components/Layout';

import Containers from './containers';
import { AppProvider } from './providers/app-provider/context';

// Styles
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import './App.css';


const queryClient = new QueryClient();


function App() {

  return (
    <SnackbarProvider>
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <AppProvider>
            <Layout>
              <Switch>
                <Route exact path="/">
                  <Containers.Breeds />
                </Route>
                <Route exact path="/dogs/:id">
                  <Containers.Dogs />
                </Route>
                <Route exact path="/my-team">
                  <Containers.MyTeam />
                </Route>
                <Route component={NotFound} />
              </Switch>
            </Layout>
          </AppProvider>
        </BrowserRouter>
      </QueryClientProvider>
    </SnackbarProvider>
  );
}

export default App;
