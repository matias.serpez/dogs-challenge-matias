// Data Access Layer
import { useQuery } from 'react-query'
import { SERVICE_DOGS_BASE_URL, SERVICE_DOGS_QTY_PAGE } from '../constants';

interface GenericDogsApiResponse<T> {
    message: T
    status: 'error' | 'success'
}

/**
 * Get list of breeds.
 */

export type IDALBreedsList = GenericDogsApiResponse<Record<string, Array<string>>>

export function useFetchBreeds() {
    const QUERY_KEY = 'BREEDS';

    return useQuery(QUERY_KEY, async () => {
        const res = await fetch(`${SERVICE_DOGS_BASE_URL}/breeds/list/all`);
        const resJSON: IDALBreedsList = await res.json();

        return resJSON.message;
    })
}

/**
 * Get list of dogs by breeds.
 */

export type IDALDogsList = GenericDogsApiResponse<Array<string>>

export function useFetchDogs(breedName: string, page?: number) {
    const QUERY_KEY = 'DOGS_BY_BREED';

    return useQuery([QUERY_KEY, page, breedName], async () => {
        const res = await fetch(`${SERVICE_DOGS_BASE_URL}/breed/${breedName}/images/random/${SERVICE_DOGS_QTY_PAGE}`);
        const resJSON: IDALDogsList = await res.json();

        return resJSON.message;
    })

}