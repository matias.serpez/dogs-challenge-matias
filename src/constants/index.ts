export const SERVICE_DOGS_BASE_URL = 'https://dog.ceo/api';
export const SERVICE_DOGS_QTY_PAGE = 25;

export const MAX_DOGS_PER_BREED_PER_TEAM = 3;
export const MAX_TOTAL_DOGS_PER_TEAM = 10;