// PROVIDERS ACTIONS
export enum DogDataTypes {
    RemoveDogFromTeam = 'DOG_DATA_PROVIDER_REMOVE',
    AddDogToTeam = 'DOG_DATA_PROVIDER_ADD',
    AddDogs = 'DOG_DATA_PROVIDER_ADD_DOGS',
}

// DOGS DATA
export type DogsDataPayload = {
    [DogDataTypes.AddDogs]: { dogImgs: Array<string>, breedName: string, page: number }
    [DogDataTypes.AddDogToTeam]: { dogImg: string, breedName: string }
    [DogDataTypes.RemoveDogFromTeam]: { dogImg: string, breedName: string }
};

export type DogDataProviderActions = ActionMap<DogsDataPayload>[keyof ActionMap<DogsDataPayload>];

// ALL ACTIONS TYPES
export type AppActions = DogDataProviderActions
