import _set from 'lodash/set'
import _filter from 'lodash/filter'
import _get from 'lodash/get'

import { DogDataTypes, AppActions } from "../types";

export const dogsDataReducer = (
    state: DogsDataProviderType,
    action: AppActions
) => {
    if (action.type === DogDataTypes.AddDogToTeam) {
        const payloadImg = action.payload.dogImg;
        const payloadBreedName = action.payload.breedName;

        // Get existing images and push the new one.
        const dogsInTeam = [..._get(state.team.breeds, payloadBreedName, []), payloadImg]

        // Set array with new data to state
        return Object.assign({}, state, { team: { breeds: { ...state.team.breeds, [payloadBreedName]: dogsInTeam, } } });
    }

    if (action.type === DogDataTypes.RemoveDogFromTeam) {
        const payloadImg = action.payload.dogImg;
        const payloadBreedName = action.payload.breedName;

        // Get existing images
        const existingBreedImages = _get(state.team.breeds, payloadBreedName);
        // Filter removed image
        const newBreedImages: Array<string> = _filter(existingBreedImages, img => img !== payloadImg);
        // Set array with new data to state
        return Object.assign({}, _set(state, `team.breeds.${payloadBreedName}`, newBreedImages));
    }


    if (action.type === DogDataTypes.AddDogs) {
        const { payload } = action;

        return Object.assign({}, _set(state, `dogs.${payload.breedName}.${payload.page}`, payload.dogImgs))
    }

    return state;
};