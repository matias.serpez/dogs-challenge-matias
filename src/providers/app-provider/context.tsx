// VENDOR
import React, { createContext, Dispatch } from "react";
// REDUCERS
import { dogsDataReducer } from "./reducers/dogs-data";
// TYPES
import { AppActions } from "./types";
// PERSISTENCE
import createPersistedReducer from 'use-persisted-reducer';

const usePersistedReducer = createPersistedReducer('state');

const initialState: AppProviderInitialState = {
    dogsData: {
        team: {
            breeds: {}
        },
        dogs: {},
    }
}

const AppContext = createContext<{
    state: AppProviderInitialState;
    dispatch: Dispatch<AppActions>;
}>({
    state: initialState,
    dispatch: () => null
});

const mainReducer = ({ dogsData }: AppProviderInitialState, action: AppActions) => ({
    dogsData: dogsDataReducer(dogsData, action),
});

const AppProvider: React.FC = ({ children }) => {
    const [state, dispatch] = usePersistedReducer(mainReducer, initialState);

    return (
        <AppContext.Provider value={{ state, dispatch }}>
            {children}
        </AppContext.Provider>
    );
};

export { AppContext, AppProvider };