import _map from 'lodash/map'
import { useEffect, useState } from 'react';
import { useScrollYPosition } from 'react-use-scroll-position';

export function useFormatedTeam(team: { breeds: {} | Record<string, string[]> }) {

    const formatedTeam: Array<{
        name: string,
        images: Array<string>
    }> = _map(team.breeds, (images, name) => ({
        name,
        images,
    }))

    // Totals dogs in all team.
    const teamSize = formatedTeam.reduce((stack, current) => {
        return stack += current.images.length;
    }, 0)

    return {
        formatedTeam,
        teamSize
    }
}

export function useScrollPagination() {
    const [page, setPage] = useState<number>(0);
    const scrollY = useScrollYPosition();
    const isInBottom = (window.innerHeight + scrollY) >= document.body.offsetHeight

    useEffect(() => {
        if (isInBottom) {
            setPage((oldPage) => oldPage + 1)
        }
    }, [isInBottom])

    return {
        page,
    }
}