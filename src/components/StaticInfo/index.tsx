import styled from '@emotion/styled'

interface IProps {
    text: string
}

export function StaticInfo({ text }: IProps) {
    return (
        <StyledContainer>
            <h2>{text}</h2>
        </StyledContainer>
    )
}

const StyledContainer = styled.div`
    display: flex;
    width: 100%;
    height: 200px;
    justify-content: center;
    align-items: center;
`