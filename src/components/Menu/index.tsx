import { Link } from 'react-router-dom';
import styled from '@emotion/styled';

export function Menu() {
    return (
        <StyledMenuContainer>
            <ul>
                <StyledMenuItem>
                    <StyledMenuLink to="/">Breeds</StyledMenuLink>
                </StyledMenuItem>
                <StyledMenuItem>
                    <StyledMenuLink to="/my-team">My Team</StyledMenuLink>
                </StyledMenuItem>
            </ul>
        </StyledMenuContainer>
    )
}

const StyledMenuContainer = styled.div`
    background: #dadada;
    height: 100vh;
    @media only screen and (max-width: 600px) {
        height: auto;
        padding-bottom: 16px;
    }
`
const StyledMenuItem = styled.li`
    padding-top: 16px;
    padding-left: 16px;
`
const StyledMenuLink = styled(Link)`
    width: 60px;
    text-decoration: none;
    color: #333333;
    background: #dadada;
`