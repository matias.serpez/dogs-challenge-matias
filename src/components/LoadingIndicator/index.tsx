import Loader from "react-loader-spinner";
import styled from '@emotion/styled'

export function LoadingIndicator() {
    return (
        <StyledLoaderContainer>
            <Loader
                type="ThreeDots"
                color="#2196f3"
                height={100}
                width={100}
            />
        </StyledLoaderContainer>
    );
}

const StyledLoaderContainer = styled.div`
    width: 100%;
    margin-top: 5%;
    display: flex;
    justify-content: center;
    align-items: center;
`