import { Link } from 'react-router-dom'
import styled from '@emotion/styled'

interface IProps {
    title?: string
    imageUrl?: string
    btnClick?: () => void
    btnLabel?: string
    link?: string
    linkLabel?: string
}

export function GridItem({ title, btnClick, btnLabel, imageUrl, link, linkLabel }: IProps) {
    return (
        <StyledGridRoot>
            <StyledGridItem>
                {imageUrl && <StyledImg src={imageUrl} alt={title} />}
                {title && <StyledTitle>{title}</StyledTitle>}
                <StyledGridFooter>
                    {link && <StyledLink to={link}>{linkLabel}</StyledLink>}
                    {(btnClick && btnLabel) && <StyledBtn onClick={btnClick}>{btnLabel}</StyledBtn>}
                </StyledGridFooter>
            </StyledGridItem>
        </StyledGridRoot>
    )
}

const StyledGridRoot = styled.div`
    width: 20%;
    @media only screen and (max-width: 600px) {
        width: 50%;
    }
`
const StyledGridItem = styled.div`
    background: #efefef;
    border-radius: 5px;
    margin: 10px;
    padding: 5px;
    display: flex;
    align-items: center;
    flex-direction: column;
`
const StyledTitle = styled.h1`
    width: 100%;
    font-size: 20px;
    text-transform: capitalize;
    height: 40px;
`
const StyledImg = styled.img`
    width: 100%;
    height: auto;
`
const StyledGridFooter = styled.div`
    width: 100%;
    display: flex;
    justify-content: flex-end;
    margin-top: 8px;
`
const StyledBtn = styled.button`
    text-decoration: none;
    border: none;
    color: #333333;
    cursor: pointer;
`
const StyledLink = styled(Link)`
    text-decoration: none;
    color: #333333;
    cursor: pointer;
`