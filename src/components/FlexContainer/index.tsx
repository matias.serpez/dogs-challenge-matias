import styled from '@emotion/styled';
import React from 'react';

export const FlexContainer: React.FC = ({ children }) => {
    return <StyledRoot>{children}</StyledRoot>
}

const StyledRoot = styled.div`
    display: flex;
    flex-wrap: wrap;
`