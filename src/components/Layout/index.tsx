import styled from '@emotion/styled';

import { FlexContainer } from "../FlexContainer"
import { Menu } from "../Menu"

// TODO: Replace inline styles
export const Layout: React.FC = ({ children }) => {
    return (
        <div>
            <StyledAppBar>
                <h1>Greencode - Dogs Challenge</h1>
            </StyledAppBar>
            <StyledContent>
                <div style={{ flex: 1, }}><Menu /></div>
                <div style={{ flex: 4, marginTop: 8 }}>
                    {children}
                </div>
            </StyledContent>
        </div>

    )
}

const StyledAppBar = styled.div`
   height: 60px;
   width: 100%;
   background: #2196f3;
   display: flex;
   align-items: center;

   h1 {
    color: white;
    font-weight: 300;
    margin-left: 16px;
    font-size: 22px;
   }
`

const StyledContent = styled.div`
    display: flex;

    @media only screen and (max-width: 600px) {
        display: initial;
    }
`